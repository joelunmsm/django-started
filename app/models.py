from django.db import models
import datetime
from django.contrib.auth.models import AbstractUser
from django.conf import settings

from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)


class MyUserManager(BaseUserManager):
    def create_user(self, username, date_of_birth, password=None):

        if not username:
            raise ValueError('Users must have an username')

        user = self.model(
            username=username
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username=None, email=None, password=None):
        user = self.create_user(
            username = username,
            password=password,
            date_of_birth="2020-01-01"
        )
        return user


# Create your models here.
class MyUser(AbstractUser):
    anexo = models.CharField(blank=False, max_length=100,default='')
    username = models.CharField(blank=False, max_length=100,default='',unique=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=True)

    objects = MyUserManager()

    USERNAME_FIELD = 'username'

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    class Meta:
        managed = True
        db_table = 'auth_user'
        ordering = ['-id']
        verbose_name = 'Usuarios'
        verbose_name_plural = 'Usuarios'