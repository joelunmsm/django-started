import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")

import django
django.setup()

from django.core.management import call_command
from django.contrib.auth.models import User
from app.models import MyUser

try:
	u = MyUser(username='root')
	u.set_password('rosa0000')
	u.is_superuser = True
	u.save()
except:
	print('root ya existe')